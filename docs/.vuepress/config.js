module.exports = {
  title: 'Jerry\'s Official Site',
  description: 'Jerry\'s Official Site',
  base: '/',
  theme: 'homesite',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Blog', link: 'https://blog.jerryqi.cn/' },
      { text: 'Gitee', link: 'https://gitee.com/jerryqi' },
      { text: 'Gihub', link: 'https://github.com/jerryqii' },
    ]
  }
}