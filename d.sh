#!/usr/bin/env sh
set -e

# Build Content
npm run build
cp README.md docs/.vuepress/dist/README.md
cd docs/.vuepress/dist
echo "jerryqi.cn" > CNAME

cd -
dd=$(date '+%Y-%m-%d %H:%M:%S')
deploycomment="Deploy at ${dd}"

# Push Code to Gitee
git add -A
git commit -m "$deploycomment"
git push origin master

# As GitHub can not serve subfolder, here use subtree to push dist to GitHub
# git subtree add --prefix=docs/.vuepress/dist git@github.com:jerryqii/jerryqii.github.io.git master --squash
git subtree push --prefix=docs/.vuepress/dist git@github.com:jerryqii/jerryqii.github.io.git master

cd -

# Reference
# https://www.jianshu.com/p/d42d330bfead
# git subtree add --prefix=docs/.vuepress/dist git@github.com:jerryqii/jerryqii.github.io.git master --squash
# git subtree pull --prefix=docs/.vuepress/dist git@github.com:jerryqii/jerryqii.github.io.git master --squash
# git subtree push --prefix=docs/.vuepress/dist git@github.com:jerryqii/jerryqii.github.io.git master
# git remote add -f github git@github.com:jerryqii/jerryqii.github.io.git
# git subtree add --prefix=docs/.vuepress/dist github master --squash
# git subtree pull --prefix=docs/.vuepress/dist github master --squash
# git subtree push --prefix=docs/.vuepress/dist github master
